import React, {Component} from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import getCookie from './components/getcookie';
import Home from './components/Home';
import Navigation from './components/Navigation';
import Search from './components/Search';
import Login from './components/Login';
import Logout from './components/Logout';
import Wishlists from './components/Wishlists';
import CreateWishlist from './components/CreateWishlist';
import SingleWishlist from './components/SingleWishlist';
import EditWishlist from './components/EditWishlist';
import DeleteWishlist from './components/DeleteWishlist';
import Registration from './components/RegistrationPage';
import RemoveGameFromWishlist from './components/RemoveGameFromWishlist';
import ShareWishlist from './components/ShareWishlist';
import ForgottenPassword from './components/ForgottenPassword';
import ResetPassword from './components/ResetPassword';
import PublicWishlists from './components/PublicWishlists';

class App extends Component {

  state = {
    authenticated: false, token: null
  }

  componentWillMount() {
    this.updateAuthenticatedState()
  }

   updateAuthenticatedState = () => {
    console.log("Authenticated function called")
    var cookie_token = getCookie("token");
    if (cookie_token === "" || cookie_token == "undefined") {
      if (this.state.authenticated != false) {
      this.setState({authenticated: false})}
      return;
    }    
    fetch('http://boardgamewishlist.iljabelankins.co.uk/api/user', {
              method:'GET',
              headers: {
                'token': cookie_token,
              }
            })
            .then((res) => {
              if(res.status == 403) {throw "invalid"}
              return res
            })
            .then(res => res.json())
            .then((jsondata) => {
              console.log("check")
              if (this.state.authenticated != true){this.setState({authenticated: true})}
            })
            .catch(err => {
              console.log('error')
              document.cookie = "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
              if (this.state.authenticated != false){ 
                this.setState({authenticated: false});
              }
            });
  }

  render () {
    return(
      <BrowserRouter>
        <div>
          <Navigation authenticated={this.state.authenticated}/>
          <div className="page-bg">
            <Switch>
              <Route path="/" component={() => (<Home authentication_function={this.updateAuthenticatedState} authenticated={this.state.authenticated} />)} exact/>
              <Route path="/search" component={() => (<Search authentication_function={this.updateAuthenticatedState} authenticated={this.state.authenticated} />)}/>
              <Route path="/registration" component={() => (<Registration authentication_function={this.updateAuthenticatedState} authenticated={this.state.authenticated} />)}/>
              <Route path="/login" component={() => (<Login authentication_function={this.updateAuthenticatedState} authenticated={this.state.authenticated} />)}/>
              <Route path="/logout" component={() => (<Logout authentication_function={this.updateAuthenticatedState} authenticated={this.state.authenticated} />)}/>
              <Route path="/reset-password/:reset_token" component={() => (<ResetPassword authentication_function={this.updateAuthenticatedState} authenticated={this.state.authenticated} />)}/>
              <Route path="/reset-password" component={() => (<ForgottenPassword authentication_function={this.updateAuthenticatedState} authenticated={this.state.authenticated} />)}/>
              <Route path="/share-wishlist/:id/" component={ShareWishlist} />
              <Route path="/public-wishlists/" component={PublicWishlists} />
              <Route path="/wishlist/:id/:boardgame_id/delete" component={RemoveGameFromWishlist} />
              <Route path="/wishlist/:id/edit" component={EditWishlist} />
              <Route path="/wishlist/:id/delete" component={DeleteWishlist} />
              <Route path="/wishlist/:id" component={() => (<SingleWishlist authentication_function={this.updateAuthenticatedState} authenticated={this.state.authenticated} />)}/>
              <Route path="/wishlist" component={() => (<Wishlists authentication_function={this.updateAuthenticatedState} authenticated={this.state.authenticated} />)}/>
              <Route path="/create-a-wishlist" component={() => (<CreateWishlist authentication_function={this.updateAuthenticatedState} authenticated={this.state.authenticated} />)}/>
              <Route component={Error}/>
            </Switch>
            </div>
        </div>
      </BrowserRouter>
    )

  }
}

export default App;
