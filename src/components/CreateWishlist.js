import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import getCookie from './getcookie';

class CreateWishlist extends Component {

    constructor(props) {
        super(props);
        this.state = { name: null, visibility: "PUBLIC", token: null, success: false };
    }

    componentWillMount() {
        var cookie_token = getCookie("token");
        if (cookie_token === "") {
          return;
        }
        this.setState({token: cookie_token})
        this.props.authentication_function()
      }

    handleChange = (event) => {
        console.log(event.target.value)
        this.setState({[event.target.name]: event.target.value});
    }

    handleSubmit = (event) => {

        var data = {name: this.state.name, visibility: this.state.visibility}
        fetch('http://boardgamewishlist.iljabelankins.co.uk/api/wishlist', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            "token" : this.state.token
          },
          body: JSON.stringify(data)
          }).then(function(response) {
              return response.json();
          })
          .then((response) => {this.setState({["success"]: true})})
          .catch(console.log);
        
        
        
        event.preventDefault();
      
    }

    render(){
        if (this.props.authenticated == false || this.state.success == true) {
            return(
                <Redirect to="/" />
            );
        }
        return(
        <div className="wrapper">
            <h1>Create a Wishlist</h1>
            <form onSubmit={this.handleSubmit}>
            <fieldset>
                <label>
                    <p>Wishlist Name</p>
                    <input type="text" value={this.state.name} name="name" onChange={this.handleChange} />

                    <label for="visibility">Choose a visibility: </label>
                    <select name="visibility" id="visibility" value={this.state.visibility} onChange={this.handleChange} >
                        <option value="PUBLIC">Public</option>
                        <option value="PRIVATE">Private</option>
                    </select>
                </label>
            </fieldset>
            <input type="submit" value="Submit" />
            </form>
        </div>
    )};
}

export default CreateWishlist