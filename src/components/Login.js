import React, { Component } from 'react';
import { Redirect, NavLink } from 'react-router-dom'


class Login extends Component {

    constructor(props) {
        super(props);
        this.state = { username: '', password: '', token: null};
    }

    handleChange = (event) => {
        console.log(event.target.value)
        this.setState({[event.target.name]: event.target.value});
    }

    handleSubmit = (event) => {
    
        fetch('http://boardgamewishlist.iljabelankins.co.uk/api/login?username=' + this.state.username + "&password=" + this.state.password, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
          }
          }).then(function(response) {
              console.log(response)
              return response.json();
          }).then(function(data) {
              document.cookie="token=" + data.token;
              return data;
            })
          .then((data) => {this.setState({["token"]: data.token})})
          .then(() => {this.props.authentication_function() })
          .catch(console.log);
        
        
        event.preventDefault();
      
    }

    render(){
        if (this.state.token != null) {
            return(
                <Redirect to="/" />
            );
        }
        return(
        <div className="wrapper">
            <h1>Login Form</h1>
            <form onSubmit={this.handleSubmit}>
                    <fieldset>
                        <label>
                            <p>Username:</p>
                            <input class="form-control" type="text" value={this.state.username} placeholder="Username" name="username" onChange={this.handleChange} />
                            <br></br>
                            <p>Password:</p>
                            <input class="form-control" type="password" value={this.state.password} placeholder="Password" name="password" onChange={this.handleChange} />
                        </label>
                    </fieldset>
                    <input class="btn btn-primary" type="submit" value="Submit" />
                    <br></br>
                    <br></br>
                    <NavLink to='/reset-password'>Forgot your password. </NavLink>
            </form>
        </div>
        
    )};
}

export default Login