import React, { Component } from 'react';
import { Redirect } from 'react-router-dom'


class ForgottenPassword extends Component {

    constructor(props) {
        super(props);
        this.state = { username: '', email: '', token: ''};
    }

    componentWillMount() {
        this.props.authentication_function()
      }

    handleChange = (event) => {
        console.log(event.target.value)
        this.setState({[event.target.name]: event.target.value});
    }

    handleSubmit = (event) => {
        
        var data = {username: this.state.username, email: this.state.email}
        fetch('http://boardgamewishlist.iljabelankins.co.uk/api/reset-password', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(data)
          }).then(function(response) {
              console.log(response)
              return response.json();
          })
          .catch(console.log);
        

        event.preventDefault();
      
    }

    render(){
        if (this.props.authenticated == true) {
            return(
                <Redirect to="/" />
            );
        }
        return(
        <div className="wrapper">
            <h1>Enter your username and email to reset your password. (Case sensitive)</h1>
            <form onSubmit={this.handleSubmit}>
            <fieldset>
                <label>
                    <p>Username</p>
                    <input type="text" value={this.state.username} name="username" onChange={this.handleChange} />
                    <p>Email</p>
                    <input type="email" value={this.state.email} name="email" onChange={this.handleChange} />
                </label>
            </fieldset>
            <input type="submit" value="Submit" />
            </form>
        </div>
    )};
}

export default ForgottenPassword;