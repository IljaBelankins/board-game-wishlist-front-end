import React, { Component } from 'react';

class AddGameToWishlist extends Component {

    state = {
        selected: 'select'
      }
 
    handleChange = (event) => {
            
            fetch('http://boardgamewishlist.iljabelankins.co.uk/api/wishlist/' + event.target.value + '/' + this.props.boardgame_id, {
                method: 'POST',
                headers: {
                'Content-Type': 'application/json',
                'token': this.props.token,
                },
                })
                .catch(console.log);
            
                this.addtoWishlist(event.target.value)
            event.preventDefault();
            
        }

    getWishlists = () => {
        var wishlists_to_display = [];
        if(this.props.wishlists===undefined){
            return wishlists_to_display
        }
        console.log(this.props.wishlists)
        for (var index=0; index<this.props.wishlists.length; index++){
            if(!this.props.wishlists[index].boardgame_ids.includes(this.props.boardgame_id)){
            wishlists_to_display.push(this.props.wishlists[index])
            }
        }
    return wishlists_to_display;
    }

    addtoWishlist = (wishlist_id) => {
        for (var index=0; index<this.props.wishlists.length; index++){
            if(this.props.wishlists[index].id==wishlist_id){
                console.log('hmmm')
            this.props.wishlists[index].boardgame_ids.push(this.props.boardgame_id)
            }
        }
        this.setState({selected: 'select'})
    }

    render(){
        console.log('render')
        var wishlists = this.getWishlists()
        if (wishlists.length==0) {
            return (
                <div>
                    No wishlists to use.
                </div>
            )
        }
        return (
            <div>
                <label for="listwishlists">Choose a wishlist:</label>
                <select class="form-control" id="exampleFormControlSelect1" value={this.state.selected} name="listwishlists" id="cars" onChange={this.handleChange}>
                <option selected disabled value="select">Select</option>
                {wishlists.map((wishlist) => (
                            <option value={wishlist.id}>{wishlist.name}</option>
                ))}
                </select>
            </div>
    )};
}

export default AddGameToWishlist;