import React from 'react';
import { Link } from 'react-router-dom';

const ListPublicWishlists = ({ wishlistitems }) => {
    console.log(wishlistitems)
    if(wishlistitems === undefined) {
        return(
            <div>
                Nothing is here yet.
            </div>
        )
    }
    return (
        <div>
            <hr></hr>
            {wishlistitems.map((wishlistitem) => (
                <div key={wishlistitem.id} className="card">
                    <div className="card-body">
                    <Link to={{
                            pathname: '/wishlist/' + wishlistitem.id,
                            state: {
                            wishlist_id: wishlistitem.id
                            }
                        }}>Go to wishlist.</Link>
                        <h5 className="card-title">{wishlistitem.name}</h5>
                    </div>
                </div>
            ))}
        </div>
    );
}

export default ListPublicWishlists;