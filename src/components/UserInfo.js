import React, { Component } from 'react';
import moment from 'moment';
import getCookie from './getcookie';

class UserInfo extends Component {

    state = {
        user: '', token: ''
      }

    componentWillMount() {
        var cookie_token = getCookie("token");
        if (cookie_token === "") {
          return;
        }
        this.setState({token: cookie_token})
      }

    componentDidMount() {
        console.log()
            fetch('http://boardgamewishlist.iljabelankins.co.uk/api/user', {
              method:'GET',
              headers: {
                'token': this.state.token,
              }
            })
            .then(res => res.json())
            .then((jsondata) => {
              this.setState({ user: jsondata})
            })
            .catch(console.log);
      }

    render(){
        if (this.props.authenticated != true) {
            return(
              <div>
                <p className="welcome-bg">Welcome to Boardgame Wishlist!</p>
                <p> Please login to access the full extent of our services.</p>
                </div>
            );
        }

        return(
            <div>
                <p className="welcome-bg">Welcome back {this.state.user.username}!</p> 
                <p>You have been a member for {moment().diff( moment(this.state.user.created_at).format('YYYY-MM-DD'), 'days')} days. </p>
            </div>
        );
    };
}

export default UserInfo;