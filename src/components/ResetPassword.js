import React, { Component } from 'react';
import { Redirect, withRouter } from 'react-router-dom'


class ResetPassword extends Component {

    constructor(props) {
        super(props);
        this.state = { password: '', token: '', success: false};
    }

    componentWillMount() {
        this.props.authentication_function()
      }

    handleChange = (event) => {
        console.log(event.target.value)
        this.setState({[event.target.name]: event.target.value});
    }

    handleSubmit = (event) => {

        var data = {password: this.state.password}
        fetch('http://boardgamewishlist.iljabelankins.co.uk/api/reset-password/' + this.props.match.params.reset_token, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(data)
          })
          .then((response) => {this.setState({["success"]: true})})
          .catch(console.log);
        
    
        event.preventDefault();
      
    }

    render(){
        if (this.props.authenticated == true || this.state.success == true) {
            return(
                <Redirect to="/" />
            );
        }
        return(
        <div className="wrapper">
            <h1>Enter your new password.</h1>
            <form onSubmit={this.handleSubmit}>
            <fieldset>
                <label>
                    <p>Password:</p>
                    <input type="text" value={this.state.password} name="password" onChange={this.handleChange} />
                </label>
            </fieldset>
            <input type="submit" value="Submit" />
            </form>
        </div>
    )};
}

export default withRouter(ResetPassword);