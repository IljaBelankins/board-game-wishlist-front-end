import React, { Component } from 'react';
import Carousel from 'react-multi-carousel';
import  'react-multi-carousel/lib/styles.css';


class TopBoardgames extends Component {

    state = {
        top_boardgames: []
      }

    componentDidMount() {
        console.log()
            fetch('http://boardgamewishlist.iljabelankins.co.uk/api/popular-boardgames', {
              method:'GET',
            })
            .then(res => res.json())
            .then((jsondata) => {
              this.setState({ top_boardgames: jsondata})
            })
            .catch(console.log);
      }


    render(){
        const responsive = {
            superLargeDesktop: {
              // the naming can be any, depends on you.
              breakpoint: { max: 4000, min: 3000 },
              items: 5
            },
            desktop: {
              breakpoint: { max: 3000, min: 1024 },
              items: 3
            },
            tablet: {
              breakpoint: { max: 1024, min: 464 },
              items: 2
            },
            mobile: {
              breakpoint: { max: 464, min: 0 },
              items: 1
            }
          };
            return(
                
                <div className="topboardgames">
                    <h2>Top 5 Most Popular Boardgames:</h2>
                    <Carousel responsive={responsive}>
                    {this.state.top_boardgames.map((top_boardgame) => (
                        <div key={top_boardgame.id} className="card h-100">
                            <div className="card-body">
                                <h5 className="card-title topboardgame">{top_boardgame.name}</h5>
                                <img className="boardgame-image" src={top_boardgame.image ? top_boardgame.image:'https://t3.ftcdn.net/jpg/02/51/39/02/360_F_251390200_HpnWE9F08alVK7rjflyQKKP8RYt1Vlpd.jpg'} />
                            </div>
                        </div>
                    ))}
                    </Carousel>            
                    <br></br>    
                </div>
            );
    };
}

export default TopBoardgames;