import React, { Component } from 'react';
import { withRouter, Redirect, NavLink } from 'react-router-dom';
import getCookie from './getcookie';
import ListWishlists from './ListWishlists';

class Wishlists extends Component {

    state = {
        wishlists: [], authenticated: false, token: null
      }
    
      componentWillMount() {
        var cookie_token = getCookie("token");
        if (cookie_token === "") {
          return;
        }
        this.setState({token: cookie_token})
        this.props.authentication_function()
      }

      componentDidMount() {
        console.log(this.state.search)
          if (this.props.authenticated != true) {
            return;
          }
            fetch('http://boardgamewishlist.iljabelankins.co.uk/api/wishlist', {
              method:'GET',
              headers: {
                'token': this.state.token,
              }
            })
            .then(res => res.json())
            .then((jsondata) => {
              this.setState({ wishlists: jsondata})
            })
            .catch(console.log);

          this.state.query=this.state.search;
      }


    render(){
            if (this.props.authenticated != true) {
                return(
                    <Redirect to="/" />
                );
            }
            console.log(this.state.wishlists)
            return(
              <div className="wrapper">
                  <h1>Your wishlists</h1>
                  <NavLink to='/create-a-wishlist'>Create a Wishlist </NavLink>
                  <ListWishlists wishlistitems={this.state.wishlists} />
              </div>
              )};
        }


export default withRouter(Wishlists);