import React from 'react';

const DeleteUserFromWishlist  = ({ user_id, remove_user_function }) => {

        return(
        <div className="wrapper">
            <i class="fas fa-times-circle" onClick={() => { remove_user_function(user_id) }}></i>
        </div>
    );
}

export default DeleteUserFromWishlist;