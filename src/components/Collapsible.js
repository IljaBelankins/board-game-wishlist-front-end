import React, { useState } from 'react';
import Collapse from 'react-bootstrap/Collapse';
import Button from 'react-bootstrap/Button';


const Collapsible  = ({ title, content }) => {

    const [open, setOpen] = useState(false);

            return(
                <>
                    <Button
                        onClick={() => setOpen(!open)}
                        aria-controls="example-collapse-text"
                        aria-expanded={open}
                    >
                        {title}
                    </Button>
                    <Collapse in={open}>
                        <div id="example-collapse-text">
                        {content}
                        </div>
                    </Collapse>
    </>
            );
}

export default Collapsible;