import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import getCookie from './getcookie';

class RemoveGameFromWishlist extends Component {

    constructor(props) {
        super(props);
        this.state = { success: false, delete: null, id: null };
    }

    componentWillMount() {
        var cookie_token = getCookie("token");
        if (cookie_token === "") {
          return;
        }
        this.setState({token: cookie_token})
      }

    handleChange = (event) => {
        console.log(event.target.value)
        this.setState({[event.target.name]: event.target.value});
    }

    handleSubmit = (event) => {

        if (this.state.delete != "DELETE"){
            alert("Please enter DELETE to delete.");
            event.preventDefault();
            return null;
        }

        console.log("handle working")
        console.log(this.props.location.state.wishlist_id)
        console.log(this.props.location.state.boardgame_id)

        fetch('http://boardgamewishlist.iljabelankins.co.uk/api/wishlist/' + this.props.location.state.wishlist_id + "/" + this.props.location.state.boardgame_id, {
          method: 'DELETE',
          headers: {
            'Content-Type': 'application/json',
            "token" : this.state.token
          },
          })
          .then((response) => {this.setState({["success"]: true})})
          .catch(console.log);
        
        
        
        event.preventDefault();
      
    }

    render(){
        if (this.state.success == true) {
            return(
                <Redirect to={"/wishlist/" + this.props.location.state.wishlist_id} />
            );
        }
        return(
        <div className="wrapper">
            <h1>Are you sure you wish to DELETE this game from this wishlist?</h1>
            <form onSubmit={this.handleSubmit}>
            <fieldset>
                <label>
                    <p>Enter "DELETE" to delete game from wishlist.</p>
                    <input type="text" value={this.state.delete} name="delete" onChange={this.handleChange} />
                </label>
            </fieldset>
            <input type="submit" value="Submit" />
            </form>
        </div>
    )};
}

export default RemoveGameFromWishlist;