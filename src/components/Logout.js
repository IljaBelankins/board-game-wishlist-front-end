import React, { Component } from 'react';
import { Redirect } from 'react-router-dom'


class Logout extends Component {

    componentWillMount() {
        document.cookie = "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
        this.props.authentication_function()
        
      }

    render(){
            return(
                <Redirect to="/" />
            );
    };
}

export default Logout;