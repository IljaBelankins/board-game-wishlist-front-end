import React, { Component } from 'react';
import { Redirect } from 'react-router-dom'


class Registration extends Component {

    constructor(props) {
        super(props);
        this.state = { username: '', password: '', email: '', token: null};
    }

    componentWillMount() {
        this.props.authentication_function()
      }

    handleChange = (event) => {
        console.log(event.target.value)
        this.setState({[event.target.name]: event.target.value});
    }

    handleSubmit = (event) => {

        console.log(this.state)
        
        fetch('http://boardgamewishlist.iljabelankins.co.uk/api/register', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(this.state)
          }).then(function(response) {
              console.log(response)
              return response.json();
          }).then(function(data) {
              document.cookie="token=" + data.token;
              return data;
            })
          .then((data) => {this.setState({["token"]: data.token})})
          .catch(console.log);
        

        event.preventDefault();
      
    }

    render(){
        if (this.props.authenticated == true || this.state.token != null) {
            return(
                <Redirect to="/" />
            );
        }
        return(
        <div className="wrapper">
            <h1>Registration Form</h1>
            <form onSubmit={this.handleSubmit}>
            <fieldset>
                <label>
                    <p>Username</p>
                    <input type="text" value={this.state.username} name="username" onChange={this.handleChange} />
                    <p>Password</p>
                    <input type="password" value={this.state.password} name="password" onChange={this.handleChange} />
                    <p>Email</p>
                    <input type="email" value={this.state.email} name="email" onChange={this.handleChange} />
                </label>
            </fieldset>
            <input type="submit" value="Submit" />
            </form>
        </div>
    )};
}

export default Registration;