import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

class Navigation extends Component {    
    
    render(){

        if (this.props.authenticated != true) {
            return(
            <div className="navigation-bg">
                <NavLink to='/'>Home </NavLink>
                <NavLink to='/public-wishlists'>Public Wishlists </NavLink>
                <NavLink to='/login'>Login </NavLink>
                <NavLink to='/registration'>Registration </NavLink>
                
            </div>
            );
        }

        return(
            <div className="navigation-bg">
                <NavLink to='/'>Home </NavLink>
                <NavLink to='/wishlist'>My Wishlists </NavLink>
                <NavLink to='/public-wishlists'>Public Wishlists </NavLink>
                <NavLink to='/search'>Search </NavLink>
                <NavLink to='/logout'>LOGOUT </NavLink>
                

            </div>
            );
 };
}

export default Navigation;