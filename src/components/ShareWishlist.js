import React, { Component } from 'react';
import { Redirect } from 'react-router-dom'
import getCookie from './getcookie';


class ShareWishlist extends Component {

    constructor(props) {
        super(props);
        this.state = { username: '', permission_level: '', token: null};
    }

    componentWillMount() {
        var cookie_token = getCookie("token");
        if (cookie_token === "") {
          return;
        }
        this.setState({token: cookie_token})
      }

    handleChange = (event) => {
        
        this.setState({[event.target.name]: event.target.value});
        
    }

    handleSubmit = (event) => {

        console.log(this.state)

        var data = {username: this.state.username, permission_level: this.state.permission_level}

        fetch('http://boardgamewishlist.iljabelankins.co.uk/api/share-wishlist/' + this.props.location.state.wishlist_id, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'token': this.state.token
          },
          body: JSON.stringify(data)
          })
          .catch(console.log);
        

        event.preventDefault();
      
    }

    render(){
        var isReadChecked = "";
        var isWriteChecked = "";
        if (this.state.permission_level == "READ"){
            isReadChecked = "active"
        }
        if (this.state.permission_level == "WRITE"){
            isWriteChecked = "active"
        }

        console.log(this.state.permission_level);
        return(
        <div className="wrapper">
            <h1>Share wishlist with another user.</h1>
            <form onSubmit={this.handleSubmit}>
            <fieldset>
                <label>
                    <p>Username</p>
                    <input class="form-control" type="text" value={this.state.username} name="username" onChange={this.handleChange} />
                    
                    <p>Permission Level</p>
                    <div class="btn-group btn-group-toggle">
                        <label class={"btn btn-secondary " + isReadChecked}>
                            <input type="radio" name="permission_level" value="READ" autocomplete="off" onChange={this.handleChange}/> Read
                        </label>
                        <label class={"btn btn-secondary " + isWriteChecked}>
                            <input type="radio" name="permission_level" value="WRITE"  autocomplete="off" onChange={this.handleChange}/> Write
                        </label>
                        </div>

                </label>
            </fieldset>
            <input class="btn btn-primary" type="submit" value="Submit" />
            </form>
        </div>
    )};
}

export default ShareWishlist;