import React, { Component } from 'react';
import { withRouter, Redirect, Link } from 'react-router-dom';
import getCookie from './getcookie';
import WishlistBoardGameItem from './Wishlistboardgameitem';
import Collapsible from './Collapsible';
import ShareUsersCollapsible from './ShareUsersCollapsible';
import {Container, Row, Col} from 'react-bootstrap';

class SingleWishlist extends Component {

    state = {
        wishlist: {"visibility": "PUBLIC"}, authenticated: false, token: null
      }
    
      componentWillMount() {
        var cookie_token = getCookie("token");
        if (cookie_token === "") {
          return;
        }
        this.setState({token: cookie_token})
        this.props.authentication_function()
      }

      componentDidMount() {

            this.RefreshWishlist()

      }

      RefreshWishlist () {
        var headers={}
        if (this.state.token != undefined) {
          headers={"token": this.state.token}
        }

        fetch('http://boardgamewishlist.iljabelankins.co.uk/api/wishlist/' + this.props.match.params.id, {
              method:'GET',
              headers: headers
            })
            .then(res => res.json())
            .then((jsondata) => {
              this.setState({ wishlist: jsondata})
            })
            .catch(console.log);
      }

      RemoveUserFromWishlist = (user_id) => {

        console.log(" working")

        fetch('http://boardgamewishlist.iljabelankins.co.uk/api/share-wishlist/' + this.state.wishlist.id + '/' + user_id, {
          method: 'DELETE',
          headers: {
            'Content-Type': 'application/json',
            "token" : this.state.token
          },
          })
          .then((response) => {this.RefreshWishlist()})
          .catch(console.log);
    }


    render(){
            
            console.log(this.props.authenticated)
            console.log(this.state.wishlist.visibility)
            if (this.props.authenticated != true && this.state.wishlist.visibility === undefined) {
                return(
                    <Redirect to="/" />
                );
            }

            if (this.props.authenticated != true && this.state.wishlist.visibility == "PUBLIC") {
              return(
                <div className="wrapper">
                    <h1>{this.state.wishlist.name}</h1>
                    <p>Visibility: {this.state.wishlist.visibility}</p>
                    <p>Created at: {this.state.wishlist.created_at}</p>
                    <WishlistBoardGameItem boardgames={this.state.wishlist.board_games} wishlist_id={this.state.wishlist.id} show_delete_button={false} />
                </div>
              )
            }

            console.log(this.state)

            if (this.state.wishlist.user_status == "OWNER"){
              return(
                <div className="wrapper">
                    <h1>{this.state.wishlist.name}</h1>
                    <Link to={{
                      pathname: '/wishlist/' + this.props.match.params.id + '/edit',
                      state: {
                        token: this.state.token,
                        wishlist: this.state.wishlist
                      }
                    }}>Edit</Link>
                    <br></br>
                    <Link to={{
                      pathname: '/wishlist/' + this.props.match.params.id + '/delete',
                      state: {
                        token: this.state.token,
                        id: this.state.wishlist.id
                      }
                    }}>DELETE</Link>
                    <Container>
                      <Row>
                    <Col><Collapsible title="Read Users." content={<ShareUsersCollapsible share_users={this.state.wishlist.read_users} type="Read" remove_user={this.RemoveUserFromWishlist}/>}  /> </Col>
                    <br></br>
                    <Col><Collapsible title="Write Users." content={<ShareUsersCollapsible share_users={this.state.wishlist.write_users} type="Write" remove_user={this.RemoveUserFromWishlist}/>}  /> </Col>
                      </Row>
                    </Container>
                    <p>Visibility: {this.state.wishlist.visibility}</p>
                    <p>Created at: {this.state.wishlist.created_at}</p>
                    <WishlistBoardGameItem boardgames={this.state.wishlist.board_games} wishlist_id={this.state.wishlist.id} show_delete_button={true}/>
                    
                </div>
              )
            }

            if (this.state.wishlist.user_status == "READ"){
              return(
                <div className="wrapper">
                    <h1>{this.state.wishlist.name}</h1>
                    <p>Visibility: {this.state.wishlist.visibility}</p>
                    <p>Created at: {this.state.wishlist.created_at}</p>
                    <WishlistBoardGameItem boardgames={this.state.wishlist.board_games} wishlist_id={this.state.wishlist.id} show_delete_button={false} />
                </div>
              )
            }

            if (this.state.wishlist.user_status == "WRITE"){
              return(
                <div className="wrapper">
                    <h1>{this.state.wishlist.name}</h1>
                    <Link to={{
                      pathname: '/wishlist/' + this.props.match.params.id + '/edit',
                      state: {
                        token: this.state.token,
                        wishlist: this.state.wishlist
                      }
                    }}>Edit</Link>
                    <br></br>
                    <Container>
                      <Row>
                    <Col><Collapsible title="Read Users." content={<ShareUsersCollapsible share_users={this.state.wishlist.read_users} type="Read" remove_user={this.RemoveUserFromWishlist}/>}  /> </Col>
                    <br></br>
                    <Col><Collapsible title="Write Users." content={<ShareUsersCollapsible share_users={this.state.wishlist.write_users} type="Write" remove_user={this.RemoveUserFromWishlist}/>}  /> </Col>
                      </Row>
                    </Container>
                    <p>Visibility: {this.state.wishlist.visibility}</p>
                    <p>Created at: {this.state.wishlist.created_at}</p>
                    <WishlistBoardGameItem boardgames={this.state.wishlist.board_games} wishlist_id={this.state.wishlist.id} show_delete_button={true}/>
                </div>
              )
            }

          return null;
      };
};


export default withRouter(SingleWishlist);