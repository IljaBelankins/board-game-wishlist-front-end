import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

class DeleteWishlist extends Component {

    constructor(props) {
        super(props);
        this.state = { success: false, delete: null };
    }


    handleChange = (event) => {
        console.log(event.target.value)
        this.setState({[event.target.name]: event.target.value});
    }

    handleSubmit = (event) => {

        if (this.state.delete != "DELETE"){
            alert("Please enter DELETE to delete.");
            event.preventDefault();
            return null;
        }

        console.log("handle working")

        fetch('http://boardgamewishlist.iljabelankins.co.uk/api/wishlist/' + this.props.location.state.id, {
          method: 'DELETE',
          headers: {
            'Content-Type': 'application/json',
            "token" : this.props.location.state.token
          },
          })
          .then((response) => {this.setState({["success"]: true})})
          .catch(console.log);
        
        
        
        event.preventDefault();
      
    }

    render(){
        if (this.state.success == true) {
            return(
                <Redirect to="/wishlist/" />
            );
        }
        return(
        <div className="wrapper">
            <form onSubmit={this.handleSubmit}>
            <fieldset>
                <label>
                    <p>Enter "DELETE" to delete wishlist.</p>
                    <input type="text" class="form-control" id="inputDelete" placeholder="DELETE..." value={this.state.delete} name="delete" onChange={this.handleChange} />
                </label>
            </fieldset>
            <input type="submit" class="btn btn-primary mb-2" value="Submit" />
            </form>
        </div>
    )};
}

export default DeleteWishlist;