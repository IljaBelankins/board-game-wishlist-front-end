import React, { Component } from 'react';
import { withRouter, Redirect } from 'react-router-dom';
import Boardgames from './boardgames';
import getCookie from './getcookie';

function UserWishlists (id, name, boardgame_ids) {
  return {id: id, name: name, boardgame_ids: boardgame_ids}
}

function getUserWishlistsFromResponseData (data) {
  var wishlists = []
  for (var i = 0; i < data.length; i++) { 
    var name=data[i].name;
    var id=data[i].id;
    var boardgame_ids=[]
    var boardgame_data=data[i].board_games;
    for (var index = 0; index < boardgame_data.length; index++) {
      boardgame_ids.push(boardgame_data[index].id)
    }
    if (data[i].user_status != "READ"){
    wishlists.push(UserWishlists(id, name, boardgame_ids))
  }
  }
  return wishlists
}

class Search extends Component {

    state = {
        boardgames: [], authenticated: false, token: null, query:null, user_wishlists:null
      }
    
      componentWillMount() {
        var cookie_token = getCookie("token");
        if (cookie_token === "") {
          return;
        }
        this.setState({token: cookie_token})
        this.props.authentication_function()
      }

      componentDidMount() {
        console.log()
          if (this.props.authenticated != true) {
            return;
          }
            fetch('http://boardgamewishlist.iljabelankins.co.uk/api/wishlist', {
              method:'GET',
              headers: {
                'token': this.state.token,
              }
            })
            .then(res => res.json())
            .then((jsondata) => {
              this.setState({ user_wishlists: getUserWishlistsFromResponseData(jsondata)})
            })
            .catch(console.log);
      }


      handleChange = (event) => {
        console.log(event.target.value)
        this.setState({[event.target.name]: event.target.value});
      }

      handleSubmit = (event) => {
          console.log(this.state.search)
          if (this.props.authenticated != true) {
            return;
          }
            fetch('http://boardgamewishlist.iljabelankins.co.uk/api/search?query=' + this.state.search, {
              method:'GET',
              headers: {
                'token': this.state.token,
              }
            })
            .then(res => res.json())
            .then((jsondata) => {
              this.setState({ boardgames: jsondata})
            })
            .catch(console.log);
    
          event.preventDefault();
          this.state.query=this.state.search;
      }

    render(){
      console.log(this.props)
            if (this.props.authenticated != true) {
                return(
                    <Redirect to="/" />
                );
            }
        
            return(
              <div className="wrapper">
                  <h1>Boardgame Search</h1>
                  <form onSubmit={this.handleSubmit}>
                  <fieldset>
                      <label>
                          <input placeholder="Search here.." class="form-control" type="text" value={this.state.search} name="search" onChange={this.handleChange} />
                      </label>
                  </fieldset>
                  <input class="btn btn-primary" type="submit" value="Submit" />
                  </form>
                  <Boardgames boardgames={this.state.boardgames} wishlists={this.state.user_wishlists} token={this.state.token} />
              </div>
              )};
        }


export default withRouter(Search);