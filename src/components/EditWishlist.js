import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

class EditWishlist extends Component {

    constructor(props) {
        super(props);
        this.state = { name: null, visibility: null, success: false };
    }

    componentWillMount() {
        this.state.name = this.props.location.state.wishlist.name;
        this.state.visibility = this.props.location.state.wishlist.visibility;
      }

    handleChange = (event) => {
        console.log(event.target.value)
        this.setState({[event.target.name]: event.target.value});
    }

    handleSubmit = (event) => {

        console.log("handle working")

        var data = {name: this.state.name, visibility: this.state.visibility}
        fetch('http://boardgamewishlist.iljabelankins.co.uk/api/wishlist/' + this.props.location.state.wishlist.id, {
          method: 'PUT',
          headers: {
            'Content-Type': 'application/json',
            "token" : this.props.location.state.token
          },
          body: JSON.stringify(data)
          }).then(function(response) {
              return response.json();
          })
          .then((response) => {this.setState({["success"]: true})})
          .catch(console.log);
        
        
        
        event.preventDefault();
      
    }

    render(){
        if (this.state.success == true) {
            return(
                <Redirect to={"/wishlist/" + this.props.location.state.wishlist.id}  />
            );
        }
        return(
        <div className="wrapper">
            <h1>Edit a Wishlist</h1>
            <form onSubmit={this.handleSubmit}>
            <fieldset>
                <label>
                    <p>Wishlist Name</p>
                    <input type="text" value={this.state.name} name="name" onChange={this.handleChange} />

                    <label for="visibility">Choose a visibility: </label>
                    <select name="visibility" id="visibility" value={this.state.visibility} onChange={this.handleChange} >
                        <option value="PUBLIC">Public</option>
                        <option value="PRIVATE">Private</option>
                    </select>
                </label>
            </fieldset>
            <input type="submit" value="Submit" />
            </form>
        </div>
    )};
}

export default EditWishlist;