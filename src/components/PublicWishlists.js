import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import ListPublicWishlists from './ListPublicWishlists';

class PublicWishlists extends Component {

    state = {
        wishlists: []
      }

      componentDidMount() {
        console.log(this.state.search)
            fetch('http://boardgamewishlist.iljabelankins.co.uk/api/public-wishlist', {
              method:'GET',
            })
            .then(res => res.json())
            .then((jsondata) => {
              this.setState({ wishlists: jsondata})
            })
            .catch(console.log);

          this.state.query=this.state.search;
      }


    render(){
            console.log(this.state.wishlists)
            return(
              <div className="wrapper">
                  <h1>Public Wishlists</h1>
                  <ListPublicWishlists wishlistitems={this.state.wishlists} />
              </div>
              )};
        }


export default withRouter(PublicWishlists);