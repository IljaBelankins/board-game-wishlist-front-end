import React from 'react';
import { Link } from 'react-router-dom';

function unicodeToChar(str) {
    var res = str.replace(/&apos;/g, "'")
    .replace(/&quot;/g, '"')
    .replace(/&gt;/g, '>')
    .replace(/&lt;/g, '<')
    .replace(/&amp;/g, '&')
    return res.replace(/&#([0-9]{1,3});/gi, function(match, numStr) {
        var num = parseInt(numStr, 10); // read num as normal number
        return String.fromCharCode(num);
    });
 }

const WishlistBoardGameItem = ({ boardgames, wishlist_id, wishlist_read, wishlist_write, show_delete_button }) => {
    console.log(boardgames)
    if (boardgames  === undefined){
        return null;
    }

    if (show_delete_button == true){
    return (
        <div>
            <hr></hr>
            {boardgames.map((boardgame) => (
                <div key={boardgame.id} className="card">
                    <div className="card-body">
                        <h5 className="card-title">{boardgame.name}</h5>
                        <img className="boardgame-image" src={boardgame.image ? boardgame.image:'https://t3.ftcdn.net/jpg/02/51/39/02/360_F_251390200_HpnWE9F08alVK7rjflyQKKP8RYt1Vlpd.jpg'} />
                        <br></br>
                        
                        <h6 className="card-subtitle mb-2 text-muted"><i class="fas fa-users"></i> {boardgame["min-players"]} - {boardgame["max-players"]} players.</h6>
                        <br></br>
                        <h6 className="card-subtitle mb-2 text-muted">Publisher: {boardgame["publisher"]}</h6>
                        <br></br>
                        <h6 className="card-subtitle mb-2 text-muted">Minumum Age Rating: {boardgame["age"]}</h6>
                        <br></br>
                        <p className="card-text-bg">{unicodeToChar(boardgame.description)}</p> 
                        <br></br>
                        <Link to={{
                            pathname: '/wishlist/' + wishlist_id + "/" + boardgame.id +'/delete',
                            state: {
                            wishlist_id: wishlist_id,
                            boardgame_id: boardgame.id
                            }
                        }}>DELETE from wishlist.</Link>
                    </div>
                </div>
            ))}
        </div>
    )};
    if (show_delete_button == false){
        return (
            <div>
                <hr></hr>
                {boardgames.map((boardgame) => (
                    <div key={boardgame.id} className="card">
                        <div className="card-body">
                            <h5 className="card-title">{boardgame.name}</h5>
                            <img className="boardgame-image" src={boardgame.image ? boardgame.image:'https://t3.ftcdn.net/jpg/02/51/39/02/360_F_251390200_HpnWE9F08alVK7rjflyQKKP8RYt1Vlpd.jpg'} />
                            <br></br>
                            
                            <h6 className="card-subtitle mb-2 text-muted"><i class="fas fa-users"></i> {boardgame["min-players"]} - {boardgame["max-players"]} players.</h6>
                            <br></br>
                            <h6 className="card-subtitle mb-2 text-muted">Publisher: {boardgame["publisher"]}</h6>
                            <br></br>
                            <h6 className="card-subtitle mb-2 text-muted">Minumum Age Rating: {boardgame["age"]}</h6>
                            <br></br>
                            <p className="card-text-bg">{unicodeToChar(boardgame.description)}</p> 
                            <br></br>
                        </div>
                    </div>
                ))}
            </div>
        )};
}

export default WishlistBoardGameItem;