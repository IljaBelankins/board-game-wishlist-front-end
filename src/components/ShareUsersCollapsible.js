import React from 'react';
import DeleteUserFromWishlist from './DeleteUserFromWishlist';


const ShareUsersCollapsible  = ({ share_users, type, remove_user }) => {

    if (share_users  === undefined || share_users.length < 1){
        return(
            <p>There are no {type} users for this wishlist.</p>
        );
    }

    return(
        <div>
        {share_users.map((share_user) => (
            <div key={share_user.id} className="card">
                <p>{share_user.username}</p>
                <DeleteUserFromWishlist user_id={share_user.id} remove_user_function={remove_user} />
            </div>
        ))};
    </div>
    )}

export default ShareUsersCollapsible;