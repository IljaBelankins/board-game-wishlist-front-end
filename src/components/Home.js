import React, { Component } from 'react';
import TopBoardgames from './TopBoardgames'
import UserInfo from './UserInfo';

class Home extends Component {

    componentWillMount() {
        this.props.authentication_function()
      }

    render(){
            return(
                <div>
                    <UserInfo authenticated={this.props.authenticated}/>
                    <TopBoardgames/>
                    <p>Bacon pipsum dolor amet beef pig kevin, kielbasa meatball biltong shoulder filet mignon salami ground round pancetta rump. Shoulder short loin shankle, pastrami picanha tenderloin rump. Short ribs hamburger meatloaf, porchetta tail prosciutto swine chislic strip steak turducken ground round t-bone doner. Ground round pork belly filet mignon bresaola jerky porchetta.</p>
                    <p>Prosciutto capicola chuck buffalo pastrami kielbasa tongue alcatra. Spare ribs doner pork chop swine pancetta hamburger. Picanha landjaeger pork belly porchetta beef, bacon sausage pancetta tail hamburger kielbasa tenderloin rump tongue. Bresaola meatball ham pig kevin. Tail porchetta turducken ham hock bresaola ribeye, tri-tip chislic jowl chicken. Bresaola beef ribs rump short ribs chislic turkey pig biltong venison tail hamburger.</p>
                </div>
            );
    };
}

export default Home;